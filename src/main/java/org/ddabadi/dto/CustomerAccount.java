package org.ddabadi.dto;

import java.util.List;

public class CustomerAccount {
    public String customerName;
    public String customerPhone;
    public List<AccountData> accounts;

    public CustomerAccount(String customerName, String customerPhone, List<AccountData> accounts) {
        this.customerName = customerName;
        this.customerPhone = customerPhone;
        this.accounts = accounts;
    }
}
