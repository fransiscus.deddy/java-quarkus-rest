package org.ddabadi.client;

import org.ddabadi.dto.AccountData;
import org.eclipse.microprofile.rest.client.annotation.ClientHeaderParam;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@RegisterRestClient(configKey = "account-service")
public interface AccountService {

    @GET
    @Path("account/list")
    @Produces(MediaType.APPLICATION_JSON)
    @ClientHeaderParam(name = "x-secret-key", value = "112233")
    List<AccountData> getAccount(
            @QueryParam("id") String id,
            @HeaderParam("x-secret-key-2") String secretKey2
    );

}
