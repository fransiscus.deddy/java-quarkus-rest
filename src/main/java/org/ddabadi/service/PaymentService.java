package org.ddabadi.service;

import org.ddabadi.iface.PaymentInterface;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class PaymentService implements PaymentInterface {

    @Override
    public void executePay(){

        System.out.println("Payment execute !");
    }

}
