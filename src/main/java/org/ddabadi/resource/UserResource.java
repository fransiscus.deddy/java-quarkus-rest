package org.ddabadi.resource;

import org.ddabadi.dto.UserData;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.util.*;

@Path("user")
public class UserResource {

    private Map<String,UserData> userDatas = new HashMap<>();
    @Path("all")
    @GET
    public Response getAllUser(){
//        List<UserData> users = List.of(new UserData("deddy","deddu@deddy.com"));

        return Response.ok(userDatas).build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createUser(UserData userData){
        userDatas.put(userData.getEmail(), userData);
        return Response.created(URI.create("/user/"+userData.getEmail())).build();
    }

    @GET
    @Path("{email}")
    public Response getByEmail(@PathParam("email") String email) {

        UserData userData = userDatas.get(email);
        if (Objects.isNull(userData)){
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        return Response.ok(userData).build();
    }
}
