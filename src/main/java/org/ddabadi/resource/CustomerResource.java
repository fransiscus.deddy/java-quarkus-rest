package org.ddabadi.resource;

import org.ddabadi.client.AccountService;
import org.ddabadi.dto.AccountData;
import org.ddabadi.dto.CustomerAccount;
import org.eclipse.microprofile.rest.client.inject.RestClient;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("customer")
public class CustomerResource {

    @RestClient
    AccountService accountService;

    @GET
    @Path("my-account")
    @Produces(MediaType.APPLICATION_JSON)
    public CustomerAccount getMyAccount(){
        List<AccountData> accountDatas= accountService.getAccount("123","123456789");
        CustomerAccount customerAccount = new CustomerAccount("deddy","081212313",accountDatas);
        return customerAccount;
    }


}
