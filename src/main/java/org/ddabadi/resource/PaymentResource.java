package org.ddabadi.resource;

import org.ddabadi.iface.PaymentInterface;
import org.ddabadi.service.CustomerService;
import org.ddabadi.service.PaymentService;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

@Path("payment")
public class PaymentResource {

    @Inject
    PaymentInterface paymentService;

    @Inject
    CustomerService customerService;

    @Inject
    Integer Age;

    @GET
    public Response payment(){
        System.out.println("umur "+ Age);
        customerService.getCustomer();
        paymentService.executePay();
        return  Response.ok("success").build();
    }
}
